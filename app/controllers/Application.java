package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

    public static Result index() {
      String user = session("connected");
      if(user != null){
        return ok(index.render("A user is loged"));
      } else{
        return ok(index.render("Welcome!"));
      }
    }

}
