package controllers;

import play.*;
import play.mvc.*;
import views.html.*;
import models.*;
import play.data.*;
import forms.*;


import static play.data.Form.*;

public class LoginAdminController extends Controller {

    public static Result login() {
        return ok( views.html.login.loginAdmin.render(Form.form(LoginAdminForm.class)));
    }

    public static Result authenticate1() {
        Form<LoginAdminForm> loginAdminForm = Form.form(LoginAdminForm.class).bindFromRequest();

        if (!loginAdminForm.hasErrors()) {
            return badRequest( views.html.login.loginAdmin.render(loginAdminForm) );
        } else {
            session().clear();
            session("email","admin");
            session("type",  "user");
            return redirect(
                routes.AdminController.user()
            );
        }
    }

  
    public static Result logout(){
        session().clear();
        return redirect( routes.Application.index() );
    }
}