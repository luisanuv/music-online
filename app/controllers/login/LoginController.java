package controllers;

import play.*;
import play.mvc.*;
import views.html.*;
import models.*;
import play.data.*;
import forms.*;


import static play.data.Form.*;

public class LoginController extends Controller {

    public static Result login() {
        return ok( views.html.login.login.render(Form.form(LoginForm.class)));
    }

    public static Result authenticate() {
        Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            return badRequest( views.html.login.login.render(loginForm) );
        } else {
            session().clear();
            session("email", loginForm.get().email);
            session("type",  "user");
            return redirect(
                routes.UserController.user()
            );
        }
    }

  
    public static Result logout(){
        session().clear();
        return redirect( routes.Application.index() );
    }
}