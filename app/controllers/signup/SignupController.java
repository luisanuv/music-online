package controllers;

import play.*;
import play.mvc.*;
import views.html.*;
import models.Account;
import play.data.*;
import forms.*;
import models.*;

import static play.data.Form.*;

public class SignupController extends Controller {
  final static Form<Account> signupForm = Form.form(Account.class);

  public static Result signup() {
    return ok(views.html.signup.signup.render(signupForm));
  }

  public static Result submit() {
    Form<Account> sForm = signupForm.bindFromRequest();
    if (signupForm.hasErrors()) {
      return badRequest(views.html.signup.signup.render(signupForm));
    } else{
      Account new_user = sForm.get();
      new_user.save();
      return redirect(routes.Application.index());
    }
  }
}