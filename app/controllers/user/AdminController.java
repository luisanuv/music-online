package controllers;

import play.*;
import play.mvc.*;
import views.html.*;
import models.Account;
import play.data.*;
import forms.*;
import models.*;

import static play.data.Form.*;

public class AdminController extends Controller {
  public static Result user(){
    return ok(views.html.user.admin.render());
  }
}