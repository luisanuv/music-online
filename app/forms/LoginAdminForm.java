package forms;

import play.data.*;
import models.*;
import static play.data.Form.*;

public class LoginAdminForm {

    public String email;
    public String password;

    public String validate() {
        if (AdminAccount.authenticateAdmin(email, password) == null) {
            return "Invalid admin or password";
        }
        return null;
    }

}
