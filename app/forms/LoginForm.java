package forms;

import play.data.*;
import models.*;
import static play.data.Form.*;

public class LoginForm  {

    public String email;
    public String password;

    public String validate() {
        if (Account.authenticate(email, password) == null) {
            return "Invalid user or password";
        }
        return null;
    }

}