package models;

import play.db.ebean.Model;
import javax.persistence.*;

@Entity
@Table(name="Account")
public class Account extends Model {

    @Id
    public Long id;

    @Column(length=50, unique=true)
    public String email;

    @Column(length=30, unique=true)
    public String first_name;

    @Column(length=30, unique=true)
    public String last_name;

    @Column(length=30, unique=true)
    public String username;

    @Column(length=50)
    public String password;

    public Account(){}

    public Account(String email, String first_name, String last_name, String username, String password){
      this.email = email;
      this.first_name = first_name;
      this.last_name = last_name;
      this.username = username;
      this.password = password;
    }

    public static Finder<Long, Account> find
        = new Model.Finder<>(Long.class, Account.class);

     public static Account authenticate(String email, String password) {
        return find.where().eq("email", email)
            .eq("password", password).findUnique();
    }

}
