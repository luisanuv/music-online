package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="Admin_Account")
public class AdminAccount extends Model {

    @Id
    public Long id;

    @Column(length=50, unique=true)
    public String email;

    @Column(length=30, unique=true)
    public String first_name;

    @Column(length=30, unique=true)
    public String last_name;

    @Column(length=30, unique=true)
    public String username;

    @Column(length=50)
    public String password;

    public AdminAccount(){}

    public AdminAccount(String email, String first_name, String last_name, String username, String password){
      this.email = email;
      this.first_name = first_name;
      this.last_name = last_name;
      this.username = username;
      this.password = password;
    }

    public static Finder<Long, AdminAccount> find
        = new Model.Finder<>(Long.class, AdminAccount.class);

    public static AdminAccount authenticateAdmin(String email, String password) {
        
        return find.where().eq("email", email)
            .eq("password", password).findUnique();
    }


}
