package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name="Album")
public class Album extends Model {

    @Id
    public Long id;

    @Column(length=30, unique=true, nullable=false)
    public String name;

    @Column(nullable=false)
    public Date day;

    @ManyToOne
    public Author author_owner;

    @ManyToOne
    public Producer producer;

    @OneToMany(cascade= CascadeType.ALL)
    public List<Song> list_of_song;

    @Column
    public Long score;

}
