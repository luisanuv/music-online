package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="Artist")
public class Artist extends Model {

    @Id
    public Long id;

    @Column(length=30, unique=true, nullable=false)
    public String name;

    @Column(length=50, nullable=false)
    public String type_of_artist;

}
