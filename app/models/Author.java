package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="Author")
public class Author extends Model {

    @Id
    public Long id;

    @Column(length=50, unique=true, nullable=false)
    public String name;

    @ManyToOne(cascade= CascadeType.ALL)
    public List<Artist> list_of_artist;
}

