package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="Library")
public class Library extends Model {

    @Id
    public Long id;

    @ManyToMany
    public List<Playlist> playlists;

}