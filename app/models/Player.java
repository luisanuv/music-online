package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="Player")
public class Player extends Model {

    @Id
    public Long id;

    @ManyToMany
    public List<Playlist> playlist;

    @ManyToOne
    public Library library;

    @Column(nullable=false)
    public String state;

}