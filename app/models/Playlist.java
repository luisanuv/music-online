package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name="Playlist")
public class Playlist extends Model {

    @Id
    public Long id;

    @ManyToMany
    public List<Song> list;

    @ManyToOne
    public Account user;

    @Column(nullable=false)
    public int indice_actual;

}
