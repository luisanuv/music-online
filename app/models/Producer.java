package models;

import play.db.ebean.Model;
import javax.persistence.*;


@Entity
@Table(name="Producer")
public class Producer extends Model {

    @Id
    public Long id;

    @Column(length=30, unique=true, nullable=false)
    public String name;

    @Column(length=100, nullable=false)
    public String sello;

}
