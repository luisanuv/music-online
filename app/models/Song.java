package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Song")
public class Song extends Model {

    @Id
    public Long id;

    @Column(length=100, nullable=false)
    public String author;

    @Column(length=100, unique=true, nullable=false)
    public String title;

    @Column(nullable=false)
    public Date day;

    @Column(length=2014)
    public String lyrics_of_the_song;

    @Column(nullable=false)
    public float time_of_the_song;

    @Column(nullable=false)
    public int score;

    @Column(length=100, nullable=false)
    public String artist;

    @Column(length=150, unique=true, nullable=false)
    public String route;

    @Column(length=80, nullable=false)
    public String gender;

    @ManyToMany
    public Account user ;


}