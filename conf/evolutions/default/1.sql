# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table Account (
  id                        bigint not null,
  email                     varchar(50),
  first_name                varchar(30),
  last_name                 varchar(30),
  username                  varchar(30),
  password                  varchar(50),
  constraint uq_Account_email unique (email),
  constraint uq_Account_first_name unique (first_name),
  constraint uq_Account_last_name unique (last_name),
  constraint uq_Account_username unique (username),
  constraint pk_Account primary key (id))
;

create table Album (
  id                        bigint not null,
  name                      varchar(30) not null,
  day                       timestamp not null,
  author_owner_id           bigint,
  producer_id               bigint,
  score                     bigint,
  constraint uq_Album_name unique (name),
  constraint pk_Album primary key (id))
;

create table Artist (
  id                        bigint not null,
  name                      varchar(30) not null,
  type_of_artist            varchar(50) not null,
  constraint uq_Artist_name unique (name),
  constraint pk_Artist primary key (id))
;

create table Author (
  id                        bigint not null,
  name                      varchar(50) not null,
  constraint uq_Author_name unique (name),
  constraint pk_Author primary key (id))
;

create table Library (
  id                        bigint not null,
  constraint pk_Library primary key (id))
;

create table Player (
  id                        bigint not null,
  library_id                bigint,
  state                     varchar(255) not null,
  constraint pk_Player primary key (id))
;

create table Playlist (
  id                        bigint not null,
  cancion_actual_id         bigint,
  repetir                   boolean not null,
  repetir_una               boolean not null,
  aleatorio                 boolean not null,
  indice_actual             integer not null,
  constraint pk_Playlist primary key (id))
;

create table Producer (
  id                        bigint not null,
  name                      varchar(30) not null,
  sello                     varchar(100) not null,
  constraint uq_Producer_name unique (name),
  constraint pk_Producer primary key (id))
;

create table Song (
  id                        bigint not null,
  album_id                  bigint not null,
  author                    varchar(100) not null,
  title                     varchar(100) not null,
  day                       timestamp not null,
  lyrics_of_the_song        varchar(2014) not null,
  time_of_the_song          float not null,
  score                     integer not null,
  artist                    varchar(100) not null,
  route                     varchar(150) not null,
  gender                    varchar(80) not null,
  constraint uq_Song_title unique (title),
  constraint uq_Song_route unique (route),
  constraint pk_Song primary key (id))
;


create table Library_Playlist (
  Library_id                     bigint not null,
  Playlist_id                    bigint not null,
  constraint pk_Library_Playlist primary key (Library_id, Playlist_id))
;

create table Player_Playlist (
  Player_id                      bigint not null,
  Playlist_id                    bigint not null,
  constraint pk_Player_Playlist primary key (Player_id, Playlist_id))
;

create table Playlist_Song (
  Playlist_id                    bigint not null,
  Song_id                        bigint not null,
  constraint pk_Playlist_Song primary key (Playlist_id, Song_id))
;
create sequence Account_seq;

create sequence Album_seq;

create sequence Artist_seq;

create sequence Author_seq;

create sequence Library_seq;

create sequence Player_seq;

create sequence Playlist_seq;

create sequence Producer_seq;

create sequence Song_seq;

alter table Album add constraint fk_Album_author_owner_1 foreign key (author_owner_id) references Author (id);
create index ix_Album_author_owner_1 on Album (author_owner_id);
alter table Album add constraint fk_Album_producer_2 foreign key (producer_id) references Producer (id);
create index ix_Album_producer_2 on Album (producer_id);
alter table Player add constraint fk_Player_library_3 foreign key (library_id) references Library (id);
create index ix_Player_library_3 on Player (library_id);
alter table Playlist add constraint fk_Playlist_cancion_actual_4 foreign key (cancion_actual_id) references Song (id);
create index ix_Playlist_cancion_actual_4 on Playlist (cancion_actual_id);
alter table Song add constraint fk_Song_Album_5 foreign key (album_id) references Album (id);
create index ix_Song_Album_5 on Song (album_id);



alter table Library_Playlist add constraint fk_Library_Playlist_Library_01 foreign key (Library_id) references Library (id);

alter table Library_Playlist add constraint fk_Library_Playlist_Playlist_02 foreign key (Playlist_id) references Playlist (id);

alter table Player_Playlist add constraint fk_Player_Playlist_Player_01 foreign key (Player_id) references Player (id);

alter table Player_Playlist add constraint fk_Player_Playlist_Playlist_02 foreign key (Playlist_id) references Playlist (id);

alter table Playlist_Song add constraint fk_Playlist_Song_Playlist_01 foreign key (Playlist_id) references Playlist (id);

alter table Playlist_Song add constraint fk_Playlist_Song_Song_02 foreign key (Song_id) references Song (id);

# --- !Downs

drop table if exists Account cascade;

drop table if exists Album cascade;

drop table if exists Artist cascade;

drop table if exists Author cascade;

drop table if exists Library cascade;

drop table if exists Library_Playlist cascade;

drop table if exists Player cascade;

drop table if exists Player_Playlist cascade;

drop table if exists Playlist cascade;

drop table if exists Playlist_Song cascade;

drop table if exists Producer cascade;

drop table if exists Song cascade;

drop sequence if exists Account_seq;

drop sequence if exists Album_seq;

drop sequence if exists Artist_seq;

drop sequence if exists Author_seq;

drop sequence if exists Library_seq;

drop sequence if exists Player_seq;

drop sequence if exists Playlist_seq;

drop sequence if exists Producer_seq;

drop sequence if exists Song_seq;

